// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;

}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	float c1,c2,c3;
	c1= rand()%255;
	c2= rand()%255;
	c3= rand()%255;
	
	glColor3ub(c1,c2,c3);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
    centro.x = centro.x + velocidad.x * t;
    centro.y = centro.y + velocidad.y * t;
    	if (radio>0.1f)				//Añadida condición para que la esfera se haga más pequeña hasta un límite minimo
		{
			radio= radio - 0.01f*t;
		}
}
