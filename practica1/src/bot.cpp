#include <iostream>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>

#include "Esfera.h"
#include "DatosMemCompartida.h"
#include "MundoCliente.h"

int main(int argc, char const *argv[]) 
{	
	float posy;
	
	char *BOTmmap;
	DatosMemCompartida *PUNTDATA;

	int fdDATA=open("/tmp/datos",O_RDWR,0777);
	if(fdDATA<0) {
		perror("ERROR BOT");
	}
	BOTmmap=(char*)mmap(NULL,sizeof(*(PUNTDATA)),PROT_WRITE | PROT_READ,MAP_SHARED,fdDATA,0);
	close(fdDATA);
	PUNTDATA=(DatosMemCompartida*)BOTmmap;   //Asignacion de direccion de comienzo de BOTmmap con PUNTDATA

	while(1)
	{
		
		posy = (PUNTDATA->raqueta1.y2+ PUNTDATA->raqueta1.y1)/2;

		if(PUNTDATA->ctrl)
			exit(0);
		else if (posy<PUNTDATA->esfera.centro.y)	
			PUNTDATA->accion=1;
		else if (posy>PUNTDATA->esfera.centro.y)	
			PUNTDATA->accion=-1;
		else
			PUNTDATA->accion=0;
			
		usleep(25);
	}

	munmap(BOTmmap,sizeof(*(PUNTDATA)));  
	return 0;
	
}
