// MundoCliente.cpp.implementation of the Cmundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <fcntl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <unistd.h>

#include <iostream>
#include <sys/stat.h> 



char *PUNTmmap;    //puntero para proyeccion en memoria

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	
	munmap(PUNTmmap,sizeof(DATA));
	
	//Cierre de tubería Cliente-servidor
    close(fdClien);
    unlink("/tmp/fifoclient");
    
    //Cierre tubería DATOS
	close(fdC_Tecla);
    unlink("/tmp/fifoteclas");
    
    //close(logger);   //ELIMINAMOS LOGGER
  
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=6+2*rand()/(float)RAND_MAX;  //Aquí he modificado la velocidad inicial de la esfera
		esfera.velocidad.y=6+2*rand()/(float)RAND_MAX;
		puntos2++;
		esfera.radio=0.5f;
		
		/*sprintf(mens,"Jugador 2 marca 1 punto, lleva un total de %d puntos.\n", puntos2);  //ELIMINAMOS LOGGER
		write(logger,mens,120);
		
		if (puntos2==3){				//Si llegamos a tres puntos se cierra el programa
			
			sprintf(mens, "Ha ganado el jugador2.\n");
			write(logger,mens,120);
			exit(0);
			}
			
	*/
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		esfera.radio=0.5f;
		
		//sprintf(mens,"Jugador 1 marca 1 punto, lleva un total de %d puntos.\n", puntos1); //imprimiendo en char mens
		//write(logger,mens,120); 														  //escribo en la tubería el contenido de char mens
		
		
		/*if (puntos1==3){				//Si llegamos a tres puntos se cierra el programa
			
			sprintf(mens, "Ha ganado el jugador1.\n");
			write(logger,mens,120);
			exit(0);
	*/
	}
	
		
		//MOVIMIENTO RAQUETA
		if(PUNTDATA->accion==1)
			OnKeyboardDown('w',0,0);
		
		else if(PUNTDATA->accion==-1)
			OnKeyboardDown('s',0,0);
			
		PUNTDATA->esfera=esfera;
		PUNTDATA->raqueta1=jugador1;
		 
			
		//RECEPCION DATOS DE SERVIDOR
		read(fdClien,mensClien,200);
		sscanf(mensClien,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2);
		
		
		//Cierre de cliente al finalizar el juego
		if(puntos1==3 || puntos2==3){
			PUNTDATA->ctrl=true;
			exit(0);
		}
			

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	sprintf(Tecla,"%c", key);
	write(fdC_Tecla,Tecla,10);
}


void CMundo::Init()
{

	

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
	
	
	//logger= open("/tmp/fifolog",O_WRONLY);    //ELIMINAMOS LA PARTE DEL LOGGER(CLIENTE SE COMUNICA CON BOT)
	  
	
	//MMAP Y DIRECCIONES 
	
	int fdDATA=open("/tmp/datos",O_RDWR|O_CREAT|O_TRUNC,0777);  //fichero de tamaño de atributo DatosMemCompartida
	write(fdDATA,&DATA,sizeof(DATA));
	
	PUNTmmap=(char*)mmap(NULL,sizeof(DATA),PROT_READ|PROT_WRITE,MAP_SHARED,fdDATA,0);  //proyeccion fichero en memoria(mmap)
	
	close(fdDATA);
	
	PUNTDATA=(DatosMemCompartida*)PUNTmmap;  //asignacion direcciones
	PUNTDATA->accion=0;
	PUNTDATA->ctrl=false;
	
	
	
	//CREACION FIFO TECLAS
	mkfifo("/tmp/fifoteclas", 0777);
	fdC_Tecla = open("/tmp/fifoteclas",O_WRONLY);
	
	//CREACION FIFO DATOS
	mkfifo("/tmp/fifoclient", 0777);
	fdClien = open("/tmp/fifoclient", O_RDONLY);
	
}

