#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

int main(){
	int fd;
	char mens[120];
	mkfifo("/tmp/fifolog", 0777);
	fd = open("/tmp/fifolog", O_RDONLY);
	while(read(fd,mens,120)){
		printf("%s",mens);
	}
	close(fd);
	unlink("/tmp/fifolog");
}

